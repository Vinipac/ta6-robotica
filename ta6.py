import math as math
import random

class Coordinate:
    def __init__(self, filled, x, y):
        self.filled = filled # chance de ser considerado visto com filled, de 0 a 1
        self.x = x
        self.y = y
        self.timesObserved = 0
        self.timesObservedAsFilled = 0


map = [
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    [1.0, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1, 1.0],
    [1.0, 1.0, 1.0, 1.0, 0.1, 0.0, 0.1, 1.0, 1.0, 1.0, 1.0],
    [1.0, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1, 1.0],
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    [1.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 1.0],
    ]

numRows = 11
numCols = 11

global observationCounter
observationCounter = 0

random.seed()

sensorPositionX = 3
sensorPositionY = 5
sensorPositionAngle = 0 #degrees

def symbolCoordinate(coord, isFinalMap):
    timesObserved = coord.timesObserved

    if (isFinalMap):
        if (coord.timesObserved > 0):
            observedRatio = (coord.timesObservedAsFilled / coord.timesObserved)
            if (observedRatio >= 0.85): return ' X '
        else:
            return ' - '
        return '   '
    else:
        if ((sensorPositionX == coord.x) and (sensorPositionY == coord.y)): 
            return '   S   '.format()
        if (coord.timesObserved > 0):

            observedRatio = (coord.timesObservedAsFilled / coord.timesObserved)
            return '|' + f'{observedRatio:.3f}' + '|'
        return '   -   '

def printMatrix(matrix, isFinalMap = False):
    printString = ''
    for y in range(numCols):
        for x in range(numRows):
            coord = matrix[x][y]
            symbol = symbolCoordinate(coord, isFinalMap)
            printString += ' '
            printString += symbol
        printString += '\n'
    print(printString)

def printMatrixMap(matrix):
    printString = ''
    for y in range(numCols):
        for x in range(numRows):
            coord = matrix[x][y]
            symbol = symbolCoordinate(coord)
            printString += ' '
            printString += symbol
        printString += '\n'
    print(printString)

def observe(matrix, angle):
    observeCoordinateX = sensorPositionX + 0.0
    observeCoordinateY = sensorPositionY + 0.0

    xSpeed = round((math.sin(math.radians(angle))), 2)
    ySpeed = round((math.cos(math.radians(angle))), 2)

    while(True):
        observeCoordinateX += xSpeed
        observeCoordinateY += ySpeed

        if (observeCoordinateX >= numCols): break
        if (observeCoordinateY >= numRows): break
        if (observeCoordinateX < 0): break
        if (observeCoordinateY < 0): break

        observedCoord = matrix[math.floor(observeCoordinateY)][math.floor(observeCoordinateX)]

        observedCoord.timesObserved += 1

        # confere se o numero foi identificado como filled com bance na sua chance
        rng = random.random()

        if (rng <= observedCoord.filled):
            observedCoord.timesObservedAsFilled += 1
            break


def observe360(matrix):
    global observationCounter
    increments = 1 # angle
    totalRotations = int(360 / increments)

    tempAngle = sensorPositionAngle

    for x in range(totalRotations):
        observe(matrix, tempAngle)
        tempAngle += increments

    observationCounter += 1
    print(f'Observação: {observationCounter}')
    printMatrix(matrix)




matrix = [[Coordinate(map[x][y], x, y) for x in range(numCols)] for y in range(numRows)]


observe360(matrix)
sensorPositionX += 1
observe360(matrix)
sensorPositionX += 1
observe360(matrix)
sensorPositionX += 1
observe360(matrix)
sensorPositionX += 1
observe360(matrix)
sensorPositionX += 1
observe360(matrix)

print('Mapa final:\n')
printMatrix(matrix, True)



